

var valores = [true, 5, false, "hola", "adios", 2];

textoMasLargo(valores);
document.write(obtenerBooleano(true, valores) + ": opcion true || false <br>");
document.write(obtenerBooleano(false, valores) + ": opcion true && false <br>");
realizaCalculos(valores);


function textoMasLargo(valores) {
    var texto = new Array();
    valores.forEach(element => {
        if (typeof element === 'string') {
            texto.push(element);
        }
    });
    var larg = 0;
    var ind = 0;
    for (var i = 0; i < texto.length; i++) {
        if (texto[i].length > larg) {
            larg = texto[i].length;
            ind = i;
        }
    };
    document.write("El texto mas largo es " + texto[ind] + "<br>");
}

function obtenerBooleano(booleano, valores) {
    var bool = new Array();
    valores.forEach(element => {
        if (typeof element === 'boolean') {
            bool.push(element);
        }
    });
    if (booleano) {
        return (bool[0] || bool[1]);
    } else {
        return (bool[0] && bool[1]);
    }
}

function realizaCalculos(valores) {
    var nums = new Array();
    valores.forEach(element => {
        if (typeof element === 'number') {
            nums.push(element);
        }
    });
    document.write(nums[0] + " + " + nums[1] + " = " + (nums[0] + nums[1]) + "<br>");
    document.write(nums[0] + " - " + nums[1] + " = " + (nums[0] - nums[1]) + "<br>");
    document.write(nums[0] + " * " + nums[1] + " = " + (nums[0] * nums[1]) + "<br>");
    document.write(nums[0] + " / " + nums[1] + " = " + (nums[0] / nums[1]) + "<br>");
    document.write(nums[0] + " % " + nums[1] + " = " + (nums[0] % nums[1]) + "<br>");
}